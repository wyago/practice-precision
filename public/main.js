var radius = 0;
var time = 0;
var points = 0;

function easy() {
	radius = 100;
	time = 2;
	start();
}

function medium() {
	radius = 40;
	time = 2;
	start();
}

function hard() {
	radius = 20;
	time = 2;
	start();
}

function start() {
	var hover = document.getElementsByClassName("hover")[0];
	hover.parentNode.removeChild(hover);
	
	test();
}

function test() {
	var svg = document.getElementById("display");
	var circle = document.createElementNS("http://www.w3.org/2000/svg", "circle");
	
	var bound = svg.getBoundingClientRect();
	circle.setAttribute("cx",Math.random() * (bound.width - radius * 2) + radius);
	circle.setAttribute("cy",Math.random() * (bound.height - radius * 2) + radius);
	circle.setAttribute("r", radius);
	
	var cancelToken = window.setTimeout(fail, time * 1000);
	
	circle.onclick = function() {
		window.clearTimeout(cancelToken);
		svg.children = [];
		succeed();
	};
	svg.appendChild(circle);
}

function clear() {
	var svg = document.getElementById("display");
	while (svg.firstChild) {
		svg.removeChild(svg.firstChild);
	}
}

function fail() {
	clear();
	test();
	radius += 1;
	radius *= 1.1;
	document.getElementsByTagName("body")[0].style["background-color"] = "#FDD";
}

function succeed() {
	clear();
	test();
	radius -= 1;
	radius *= 0.9;
	points += Math.floor(100 / radius);
	document.getElementById("points").innerHTML = points + "";
	document.getElementsByTagName("body")[0].style["background-color"] = "#DFD";
}